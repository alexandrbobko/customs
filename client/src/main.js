import Vue from "vue";
import App from "./App.vue";

import BootstrapVue from "bootstrap-vue";
import { Navbar, Form } from "bootstrap-vue/es/components";

import VModal from "vue-js-modal";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faTrash, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import router from "./router";
import store from "./store";

import "bootstrap/scss/bootstrap.scss";
import "bootstrap-vue/dist/bootstrap-vue.css";

library.add(faTrash);
library.add(faPencilAlt);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(Navbar);
Vue.use(Form);
Vue.use(VModal, { dialog: true });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
