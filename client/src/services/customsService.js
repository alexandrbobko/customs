import axios from "axios";

const uri = "/api/customs";

export default {
  getCustoms() {
    return axios.get(uri);
  },

  getCustomsById(id) {
    return axios.get(`${uri}/${id}`);
  },

  addCustoms(data) {
    return axios.post(uri, data);
  },

  deleteCustoms(id) {
    return axios.delete(`${uri}/${id}`);
  }
};
