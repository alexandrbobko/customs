package com.alexandrbobko.customs.controllers;

import com.alexandrbobko.customs.domain.dtos.CustomsDto;
import com.alexandrbobko.customs.services.CustomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/customs")
public class CustomsController {
    private final CustomsService customsService;

    @Autowired
    public CustomsController(CustomsService customsService) {
        this.customsService = customsService;
    }

    @GetMapping
    public ResponseEntity<List<CustomsDto>> getCustomsList() {
        return customsService.getCustomsList();
    }

    @GetMapping("{id}")
    public ResponseEntity<CustomsDto> getCustoms(@PathVariable Integer id) {
        return customsService.getCustoms(id);
    }

    @PostMapping
    public ResponseEntity<CustomsDto> addCustoms(@RequestBody @Valid CustomsDto customsDto, BindingResult bindingResult) {
        return customsService.addCustoms(customsDto, bindingResult);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<CustomsDto> deleteCustoms(@PathVariable Integer id) {
        return customsService.deleteCustoms(id);
    }

    // TODO: Create Put function
}
