package com.alexandrbobko.customs.domain.mapping;

import com.alexandrbobko.customs.domain.Customs;
import com.alexandrbobko.customs.domain.dtos.CustomsDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CustomsMapper {

    List<CustomsDto> customsListToCustomsDtoList(List<Customs> customsList);
    CustomsDto customsToCustomsDto(Customs customs);

    Customs customsDtoToCustoms(CustomsDto customsDto);
}
