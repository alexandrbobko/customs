package com.alexandrbobko.customs.domain.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CustomsDto {
    private Integer id;

    @NotNull
    @Size(min = 2, max = 100)
    private String name;

    @NotNull
    @Size(min = 2, max = 100)
    private String country1;

    @NotNull
    @Size(min = 2, max = 100)
    private String country2;
    
    private boolean active = false;
}
