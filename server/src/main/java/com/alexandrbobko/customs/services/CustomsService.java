package com.alexandrbobko.customs.services;

import com.alexandrbobko.customs.domain.Customs;
import com.alexandrbobko.customs.domain.dtos.CustomsDto;
import com.alexandrbobko.customs.domain.mapping.CustomsMapper;
import com.alexandrbobko.customs.repositories.CustomsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

@Service
public class CustomsService {
    private final CustomsRepository customsRepository;
    private final CustomsMapper customsMapper;

    @Autowired
    public CustomsService(CustomsRepository customsRepository, CustomsMapper customsMapper) {
        this.customsRepository = customsRepository;
        this.customsMapper = customsMapper;
    }

    public ResponseEntity<List<CustomsDto>> getCustomsList() {
        List<Customs> customs = (List<Customs>) customsRepository.findAll();
        return new ResponseEntity<>(customsMapper.customsListToCustomsDtoList(customs), HttpStatus.OK);
    }

    public ResponseEntity<CustomsDto> getCustoms(Integer id) {
        Optional<Customs> customsEntry = customsRepository.findById(id);
        return customsEntry
                .map(customs -> new ResponseEntity<>(customsMapper.customsToCustomsDto(customs), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    public ResponseEntity<CustomsDto> addCustoms(CustomsDto customsDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(customsDto, HttpStatus.BAD_REQUEST);
        }

        Customs customs = customsMapper.customsDtoToCustoms(customsDto);

        Customs result = customsRepository.save(customs);
        CustomsDto resultDto = customsMapper.customsToCustomsDto(result);

        return new ResponseEntity<>(resultDto, HttpStatus.CREATED);
    }

    public ResponseEntity<CustomsDto> deleteCustoms(Integer id) {
        Optional<Customs> customsEntry = customsRepository.findById(id);
        if (!customsEntry.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Customs customs = customsEntry.get();
        customsRepository.delete(customs);

        CustomsDto customsDto = customsMapper.customsToCustomsDto(customs);
        return new ResponseEntity<>(customsDto, HttpStatus.OK);
    }
}
