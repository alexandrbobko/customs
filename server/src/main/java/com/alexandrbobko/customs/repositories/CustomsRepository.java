package com.alexandrbobko.customs.repositories;

import com.alexandrbobko.customs.domain.Customs;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomsRepository extends CrudRepository<Customs, Integer> {
}
